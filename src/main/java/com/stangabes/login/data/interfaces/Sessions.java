package com.stangabes.login.data.interfaces;

import com.stangabes.login.data.entities.User;

public interface Sessions {

    boolean isLoggedIn(String token);

    String login(String name, String pass);

    String getToken(User user);

    String getToken(String name);

    User getUser(String token);

}
