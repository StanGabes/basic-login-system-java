package com.stangabes.login.data.interfaces;

public interface UserDB {

    boolean isValid(String name, String pass);

    boolean registerUser(String name, String pass);

    boolean nameExists(String name);

}
