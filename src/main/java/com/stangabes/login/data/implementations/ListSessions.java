package com.stangabes.login.data.implementations;

import com.stangabes.login.Main;
import com.stangabes.login.data.entities.User;
import com.stangabes.login.data.interfaces.Sessions;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ListSessions implements Sessions {

    private List<User> users = new ArrayList<>();

    @Override
    public boolean isLoggedIn(String token) {
        return users.stream().filter(u -> u.getToken().equals(token)).findFirst().orElse(null) == null ? false : true;
    }

    /**
     * login an user
     * @param name The username
     * @param pass The password
     * @return The newly generated token
     */
    @Override
    public String login(String name, String pass) {
        String token = getNewToken();
        if (Main.getUserDb().isValid(name, pass)) {
            User user = new User(name, token);
            users.add(user);
            System.out.println("1");
            return token;
        } else
            return null;
    }

    @Override
    public String getToken(User user) {
        User u = users.stream().filter(us -> us.equals(user)).findFirst().orElse(null);
        if (u == null)
            return null;
        else
            return u.getToken();
    }

    @Override
    public String getToken(String name) {
        User u = users.stream().filter(us -> us.getName().equals(name)).findFirst().orElse(null);
        if (u == null)
            return null;
        else
            return u.getToken();
    }

    @Override
    public User getUser(String token) {
        return users.stream().filter(u -> u.getToken().equals(token)).findFirst().orElse(null);
    }

    private String getNewToken() {
        String uuid = UUID.randomUUID().toString();
        boolean duplicate = false;
        for (User user : users) {
            if (user.getToken().equals(uuid)) {
                duplicate = true;
                break;
            }
        }
        while (duplicate) {
            uuid = UUID.randomUUID().toString();
            boolean dupe = false;
            for (User user : users) {
                if (user.getToken().equals(uuid)) {
                    dupe = true;
                    break;
                }
            }
            if (!dupe)
                duplicate = false;
        }
        return uuid;
    }

}
