package com.stangabes.login.data.implementations;

import com.stangabes.login.data.interfaces.UserDB;
import com.stangabes.login.util.Hasher;

import java.util.HashMap;
import java.util.Map;

public class MapUserDB implements UserDB {

    private Map<String, String> users = new HashMap<>();

    @Override
    public boolean isValid(String name, String pass) {
        String hashedPass = Hasher.hash(pass);
        return users.keySet().stream().filter(u -> users.get(u).equals(hashedPass)).findAny().orElse(null) == null ? false : true;
    }

    @Override
    public boolean registerUser(String name, String pass) {
        String hashedPass = Hasher.hash(pass);
        users.put(name, hashedPass);
        return false;
    }

    @Override
    public boolean nameExists(String name) {
        return users.containsKey(name);
    }

}
