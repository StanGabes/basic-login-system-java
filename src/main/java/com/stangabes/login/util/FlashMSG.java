package com.stangabes.login.util;

public enum FlashMSG {

    LOGGED_IN("You are logged in"), UNAUTHORIZED("Unauthorized access, please login"), INVALID_LOGIN("Invalid username and/or password");

    final String message;

    FlashMSG(final String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return message;
    }

}
