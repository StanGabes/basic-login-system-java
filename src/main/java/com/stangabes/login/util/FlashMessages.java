package com.stangabes.login.util;

import spark.Request;

public class FlashMessages {

    private static final String FLASH_KEY = "flash-message";

    @Deprecated
    public static void setFlashMessage(Request req, String message) {
        req.session().attribute(FLASH_KEY, message);
    }

    public static void setFlashMessage(Request req, FlashMSG message) {
        req.session().attribute(FLASH_KEY, message.toString());
    }

    public static String getFlashMessage(Request req) {
        return req.session().attribute(FLASH_KEY);
    }

    public static String captureFlashMessage(Request req) {
        String msg = getFlashMessage(req);
        req.session().removeAttribute(FLASH_KEY);
        return msg;
    }

}
