package com.stangabes.login;

import com.stangabes.login.data.implementations.ListSessions;
import com.stangabes.login.data.implementations.MapUserDB;
import com.stangabes.login.data.interfaces.Sessions;
import com.stangabes.login.data.interfaces.UserDB;
import com.stangabes.login.util.FlashMSG;
import com.stangabes.login.util.FlashMessages;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.template.handlebars.HandlebarsTemplateEngine;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static spark.Spark.*;

public class Main {

    private static UserDB userDb;
    public static UserDB getUserDb() {
        return userDb;
    }

    private static Sessions sessions;
    public static Sessions getSessions() {
        return sessions;
    }

    private static Map<String, Map<String, Object>> models = new ConcurrentHashMap<>();

    public static void main(String[] args) {

        port(5678);

        userDb = new MapUserDB();
        UserDB users = getUserDb();

        users.registerUser("stan", "test");

        sessions = new ListSessions();

        before((req, res) -> {
            Map<String, Object> model = new HashMap<>();
            model.put("flashmsg", FlashMessages.captureFlashMessage(req));
            if (req.uri().startsWith("/panel")) {
                if (req.cookie("authtoken") == null) {
                    // Not logged in
                    res.status(403);
                    res.redirect("/");
                }
                if (!sessions.isLoggedIn(req.cookie("authtoken"))) {
                    // Not logged in
                    res.status(403);
                    res.redirect("/");
                } else {
                    model.put("user", sessions.getUser(req.cookie("authtoken")));
                }
            }
            models.put(req.session().id(), model);
        });

        afterAfter((req, res) -> {
            models.remove(req);
        });

        get("/", (req, res) -> {
            return new ModelAndView(models.get(req.session().id()), "login.hbs");
        }, new HandlebarsTemplateEngine());

        // Redirect from login form
        post("/login", (req, res) -> {
            String username = req.queryParams("username");
            String password = req.queryParams("password");
            if (username == null || password == null) {
                // TODO flash message
                return null;
            }
            String token = sessions.login(username, password);
            if (token == null) {
                // Invalid
                FlashMessages.setFlashMessage(req, FlashMSG.INVALID_LOGIN);
                res.redirect("/");
                return null;
            } else {
                // Valid login
                FlashMessages.setFlashMessage(req, FlashMSG.LOGGED_IN);
                res.cookie("authtoken", token);
                res.redirect("/panel/logged-in");
            }
            return null;
        });

        get("/panel/logged-in", (req, res) -> {
            return new ModelAndView(models.get(req.session().id()), "loggedin.hbs");
        }, new HandlebarsTemplateEngine());

    }

    private void login(String name, String pass) {

    }

    private enum LoginState {
        SUCCESS, INVALID_PASS, INVALID_USER, SERVER_ERROR;
    }

}
